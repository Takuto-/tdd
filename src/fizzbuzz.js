const fizzbuzz = (num) => {
    if (( num % 5 === 0 ) && ( num % 7 === 0) ) {
        return 'fizzbuzz'
    } 
    else if (num == null) {
        return 'Error!'
    }
    else if (num % 7 === 0) {
        return 'fizz'
    }
    else if  (num % 5 === 0){
        return 'buzz'
    }
    return ''
    
}

exports.fizzbuzz = fizzbuzz;