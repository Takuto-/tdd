const calc = (operator, num1, num2) => {

    if ((operator == null) && (num1 == null) && (num2 == null)) {
        return 'entrer un opérateur ou un chiffre valide'
    }
    else if(operator == null) {
        return 'entrer un operateur'
    }
    else if ((num1 == null) || (num2 == null )) {
        return 'entrer au moins 2 chiffres'
    }
    else if (operator === 'soustraction') {
        return (num1 - num2)
    } 
    else if ((isNaN(num1) === true) && (isNaN(num2) === true)) {
        return 'entrer uniquement des chiffres'
    }
    else if(operator !==  /[\+\-\*\/]/) {
        return "l'opérateur doit uniquement être un signe mathématique"
    }
    else if (operator === 'multiplication') {
        return (num1 * num2)
    } 
    else if (operator === 'division') {
        return (num1 / num2)
    } 
    else if (operator === 'addition') {
        return (num1 + num2)
    }

}

exports.calc = calc;