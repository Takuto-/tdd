# TDD

- Cloner ce repo
- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)
- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc, 
avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction


Le test driven development consiste à réduire les anomalies d'une application en favorisant une mise en place de tests réguliers.

Les bonnes pratiques préconise de concevoir les tests avant même de développer.

Les tests commencent par une série de tests Unitaires décrivent toutes les fonctionnalités de la fonction, confirmer la réussite des tests afin d'améliorer le code et contrôler l’absence de régression

