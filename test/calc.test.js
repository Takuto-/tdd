const expect = require('chai').expect;
const {calc} = require('../src/calc');


describe("calc", () => {
    it("Handle empty answers ", () => {
        expect(calc()).to.contain("entrer un opérateur ou un chiffre valide")
    });
    it("Should contain only numbers ", () => {
        expect(calc('+', 'OnlyNumber','OnlyNumber')).to.contain("entrer uniquement des chiffres")
    });
    it("Should contain only mathematical sign for the operator", () => {
        expect(calc('test', 10, 10)).to.contain("l'opérateur doit uniquement être un signe mathématique")
    });
    it("Should contain at least 2 number", () => {
        expect(calc('+', null, 20)).to.contain("entrer au moins 2 chiffres")
    });
}) 